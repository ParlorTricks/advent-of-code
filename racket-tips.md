# Racket tips
Below are some diagrams and explanations on how certain things function in Racket. This is my understanding and learnings, so please go easy and feel free to assist in explaining these better.

# Simple function
Check if an apple is a fruit?
```mermaid
graph LR
	A[apple] --> |input| B(is-fruit?)
	B --> |return| C[#t]
```

Check if an carrot is a fruit?

```mermaid
graph LR
	A[carrot] --> |input| B(is-fruit?)
	B --> |return| C[#f]
```

## How does [map](https://docs.racket-lang.org/reference/pairs.html?q=map#%28def._%28%28lib._racket%2Fprivate%2Fmap..rkt%29._map%29%29) work?
`map` operates similar for a `for/each` loop. It takes an input list and applies a procedure to every element, and returns a list.

```mermaid
graph LR
    A["(list apple carrot kiwi)"] --> |input| E(map)
    D[is-fruit?] --> |proc| E{map}
    E{map} --> |apple| F[#t] --> |return| I["(list #t #f #t)"]
    E{map} --> |carrot| G[#f] --> |return| I
    E{map} --> |kiwi| H[#t] --> |return| I
```

Code:
```racket
(map (lambda (x) (is-fruit? x)) (list apple carrot kiwi))
```
Return: `'(#t #f #t)`

## How does [filter](https://docs.racket-lang.org/reference/pairs.html?q=filter#%28def._%28%28lib._racket%2Fprivate%2Flist..rkt%29._filter%29%29) work?
`filter` does as the name implies, you supply it with proc to filter out what you dont want, and it returns the resulting list.
```mermaid
graph LR
    A["(list apple carrot kiwi)"] --> |input| E{filter}
    D[is-fruit?] --> |proc| E
    E --> |apple| F[#t] --> |return| I["(list apple kiwi)"]
    E --> |carrot| G[#f]
    E --> |kiwi| H[#t] --> |return| I
```

Code:
```racket
(filter is-fruit? (list apple carrot kiwi))
```
Return: `'(apple kiwi)`


## How does [foldr](https://docs.racket-lang.org/reference/pairs.html?q=filter#%28def._%28%28lib._racket%2Fprivate%2Flist..rkt%29._foldr%29%29)/[foldl](https://docs.racket-lang.org/reference/pairs.html?q=filter#%28def._%28%28lib._racket%2Fprivate%2Flist..rkt%29._foldl%29%29) work?
`foldr` and `foldl` can be compared to `reduce`. I am not good at explaining this one, but as you can see from the example below, it has taken a list of numbers, applied the proc `+` to the list, to sum them up and then returns a number as the result. The `init` value which is set to `0` is the default return value if the list is empty, which in this case it is not.

```mermaid
graph LR
	A["(list 1 2 3 4 5)"] --> |input| D{"fold(r/l)"}
	B[+] --> |proc| D
	C[0] --> |init| D
	D --> E[Is the list empty?]
	E --> |yes| F[0] --> |return| H[number]
	E --> |no| G[15] --> |return| H
```

Code:
```racket
(foldr + 0 (list 1 2 3 4 5))
```
Return: `15`