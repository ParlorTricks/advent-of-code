#lang racket
;https://adventofcode.com/2019/day/2

;instructions
;1 = addition
;2 = multiply
;99 = terminate


;original data has the following changes
;elm1 = 0
;elm2 = 0
;orginal data
;(define program-data (list 1 0 0 3 1 1 2 3 1 3 4 3 1 5 0 3 2 1 9 19 1 5 19 23 1 6 23 27 1 27 10 31 1 31 5 35 2 10 35 39 1 9 39 43 1 43 5 47 1 47 6 51 2 51 6 55 1 13 55 59 2 6 59 63 1 63 5 67 2 10 67 71 1 9 71 75 1 75 13 79 1 10 79 83 2 83 13 87 1 87 6 91 1 5 91 95 2 95 9 99 1 5 99 103 1 103 6 107 2 107 13 111 1 111 10 115 2 10 115 119 1 9 119 123 1 123 9 127 1 13 127 131 2 10 131 135 1 135 5 139 1 2 139 143 1 143 5 0 99 2 0 14 0))
;puzzle instructed to change data
(define program-data (list 1 12 2 3 1 1 2 3 1 3 4 3 1 5 0 3 2 1 9 19 1 5 19 23 1 6 23 27 1 27 10 31 1 31 5 35 2 10 35 39 1 9 39 43 1 43 5 47 1 47 6 51 2 51 6 55 1 13 55 59 2 6 59 63 1 63 5 67 2 10 67 71 1 9 71 75 1 75 13 79 1 10 79 83 2 83 13 87 1 87 6 91 1 5 91 95 2 95 9 99 1 5 99 103 1 103 6 107 2 107 13 111 1 111 10 115 2 10 115 119 1 9 119 123 1 123 9 127 1 13 127 131 2 10 131 135 1 135 5 139 1 2 139 143 1 143 5 0 99 2 0 14 0))

;iterate through the list at steps of 4. Every 4th is the command
;elm1 = command
;elm2 = input1
;elm3 = input2
;elm4 = where to store value
(define (find-output temp-data)
(for ([x (in-range 0 (length temp-data) 4)])
  (define command (list-ref temp-data x))

  (cond
    [(eq? (list-ref temp-data x) 1)
  (define input1 (list-ref temp-data (+ x 1)))
  (define input2 (list-ref temp-data (+ x 2)))
  (define output (list-ref temp-data (+ x 3)))     
     (set! temp-data (list-set temp-data output (+ (list-ref temp-data input1) (list-ref temp-data input2))))]
    [(eq? (list-ref temp-data x) 2)
  (define input1 (list-ref temp-data (+ x 1)))
  (define input2 (list-ref temp-data (+ x 2)))
  (define output (list-ref temp-data (+ x 3)))     

     (set! temp-data (list-set temp-data output (* (list-ref temp-data input1) (list-ref temp-data input2))))]
    [(eq? (list-ref temp-data x) 99)

     ]

    )
  )
(list-ref temp-data 0)  
)

(define (search-outputs i j temp-data)
  (set! temp-data (list-set temp-data 1 i))  ;set i
  (set! temp-data (list-set temp-data 2 j)) ;set j
  (find-output temp-data)
)


(for ([i (in-range 0 152 1)])
  (for ([j (in-range 0 152 1)])
    (define temp-value (search-outputs i j program-data))
    (cond
      [(eq? temp-value 19690720)
        (printf "i:~a, j:~a = ~a" i j temp-value)])))
