# Advent of Code 2019 - Day 1 - Part 1
## Day 1: The Tyranny of the Rocket Equation

https://adventofcode.com/2019/day/1

Santa has become stranded at the edge of the Solar System while delivering presents to other planets! To accurately calculate his position in space, safely align his warp drive, and return to Earth in time to save Christmas, he needs you to bring him measurements from fifty stars.

Collect stars by solving puzzles. Two puzzles will be made available on each day in the Advent calendar; the second puzzle is unlocked when you complete the first. Each puzzle grants one star. Good luck!

The Elves quickly load you into a spacecraft and prepare to launch.

At the first Go / No Go poll, every Elf is Go until the Fuel Counter-Upper. They haven't determined the amount of fuel required yet.

Fuel required to launch a given module is based on its mass. Specifically, to find the fuel required for a module, take its mass, divide by three, round down, and subtract 2.

For example:
- For a mass of 12, divide by 3 and round down to get 4, then subtract 2 to get 2.
- For a mass of 14, dividing by 3 and rounding down still yields 4, so the fuel required is also 2.
- For a mass of 1969, the fuel required is 654.
- For a mass of 100756, the fuel required is 33583.

The Fuel Counter-Upper needs to know the total fuel requirement. To find it, individually calculate the fuel needed for the mass of each module (your puzzle input), then add together all the fuel values.

What is the sum of the fuel requirements for all of the modules on your spacecraft?

## Working It Out
First lets load the libraries required to assist us. The input data is a file that contains a single column and many rows, so i chose to treat this as a CSV file. The library `csv-reading` is useful for importing the file.

```racket
#lang racket
(require csv-reading)
```

Next lets define a variable `fuel` to hold all the data in the CSV file.

```racket
(define fuel (call-with-input-file "fuel.csv" csv->list))
```

We will write a function that takes a value from a row in the CSV file and calculates the required fuel for the mass supplied. The formula we are asked to use is `(round (mass / 3))-2`, we will name this function `calc-fuel`

```racket
(define (calc-fuel mass)
  (- (floor (/ mass 3)) 2))
```

To run the function across all the mass values in the CSV, we will define another function that also utilises the `map` command. Map operates much like a for/each loop, but is more efficient from my understanding. So we will use `map` to apply `calc-fuel` to every mass value. 

We also need to convert each value we get out of the CSV to a number as the input value will be a string. The function `string->number` will do this for us. The row that is returned from the CSV is also a `list` so we need to refer to the `first` element to get our stringified number.

We are also supplying a `lambda` to map, which is like a function without a name. The whole function `adj-fuel` returns a list called `fuel`, which contains all our fuel calculations.

```racket
(define adj-fuel
  (map (lambda (v) (calc-fuel (string->number (first v))))
       fuel))
```

Last but not least we need to add up all the fuel and supply the total to pass the puzzle. I have chosen `foldr` here, which takes a procedure, and inital value and a list(s). We are supplying one list, which is the return value of the function `adj-fuel`. The interesting thing to note here is that we are using `+` as the procedure, which allows us to sum up everything easily.

```racket
(foldr + 0 adj-fuel)
```

After that runs we get the answer `3239890`.

# Advent of Code 2019 - Day 1 - Part 2

During the second Go / No Go poll, the Elf in charge of the Rocket Equation Double-Checker stops the launch sequence. Apparently, you forgot to include additional fuel for the fuel you just added.

Fuel itself requires fuel just like a module - take its mass, divide by three, round down, and subtract 2. However, that fuel also requires fuel, and that fuel requires fuel, and so on. Any mass that would require negative fuel should instead be treated as if it requires zero fuel; the remaining mass, if any, is instead handled by wishing really hard, which has no mass and is outside the scope of this calculation.

So, for each module mass, calculate its fuel and add it to the total. Then, treat the fuel amount you just calculated as the input mass and repeat the process, continuing until a fuel requirement is zero or negative. For example:

- A module of mass 14 requires 2 fuel. This fuel requires no further fuel (2 divided by 3 and rounded down is 0, which would call for a negative fuel), so the total fuel required is still just 2.
- At first, a module of mass 1969 requires 654 fuel. Then, this fuel requires 216 more fuel (654 / 3 - 2). 216 then requires 70 more fuel, which requires 21 fuel, which requires 5 fuel, which requires no further fuel. So, the total fuel required for a module of mass 1969 is 654 + 216 + 70 + 21 + 5 = 966.
- The fuel required by a module of mass 100756 and its fuel is: 33583 + 11192 + 3728 + 1240 + 411 + 135 + 43 + 12 + 2 = 50346.

What is the sum of the fuel requirements for all of the modules on your spacecraft when also taking into account the mass of the added fuel? (Calculate the fuel requirements for each module separately, then add them all up at the end.)

```racket
(define (extra-fuel fuel-mass)
  (if (<= fuel-mass 0)
      0
      (+ fuel-mass (extra-fuel (calc-fuel fuel-mass)))))
```


```
(define adj-fuel
  (map (lambda (v) (extra-fuel (calc-fuel (string->number (first v)))))
       fuel))
```


```
(foldr + 0 adj-fuel)  
```